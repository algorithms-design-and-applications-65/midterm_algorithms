/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.midterm_algorithms;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class StringtoDigits {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String input = kb.next();
        System.out.println(StrtoDi(input));
    }

    public static int StrtoDi(String input) {
        int sum = 0;
        int i = 0;
        int j = 1;
        for (i = input.length() - 1; i >= 0; i--) {
            if (i == 0 && input.charAt(i) == '-') {
                sum *= -1;
            } else if (input.charAt(i) == '1') {
                sum += 1 * j;
            } else if (input.charAt(i) == '2') {
                sum += 2 * j;
            } else if (input.charAt(i) == '3') {
                sum += 3 * j;
            } else if (input.charAt(i) == '4') {
                sum += 4 * j;
            } else if (input.charAt(i) == '5') {
                sum += 5 * j;
            } else if (input.charAt(i) == '6') {
                sum += 6 * j;
            } else if (input.charAt(i) == '7') {
                sum += 7 * j;
            } else if (input.charAt(i) == '8') {
                sum += 8 * j;
            } else if (input.charAt(i) == '9') {
                sum += 9 * j;
            }
            j *= 10;

        }

        return sum;
    }
}
