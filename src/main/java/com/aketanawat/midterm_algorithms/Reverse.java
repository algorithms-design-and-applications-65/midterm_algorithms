/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.midterm_algorithms;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Reverse {
    public static void main(String[] args) {
       Scanner kb = new Scanner(System.in);
       int count = kb.nextInt(); //รับค่าจำนวนของตัวเลขทั้งหมด
       int input[] = new int[count];//รับจำนวนเต็มตามที่countกำหนด
       for(int i = 0; i < count; i++){
           input[i] = kb.nextInt(); //รับตัวเลขจำนวนเต็มเข้า
       }
        Reversing(input); //แสดงผลลัพท์
    }
    
    public static int[] Reversing(int input[]){
        int result= 0;
            for(int i = input.length -1; i>= 0; i--){//ทำ for loop เริ่มจากตัวสุดท้ายวนกลับทีละตัว
                result=input[i];
                System.out.print(result+" "); //แสดงผล
            }
         return input;  
    } 
   
}
